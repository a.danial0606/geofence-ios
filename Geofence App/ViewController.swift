//
//  ViewController.swift
//  Geofence App
//
//  Created by Ahmad Danial bin Mohd Fajariatudin on 11/09/2019.
//  Copyright © 2019 Ahmad Danial bin Mohd Fajariatudin. All rights reserved.
//

import UIKit
import MapKit
import Reachability
import CoreLocation

class ViewController: UIViewController, AddDataTableViewControllerDelegate, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var currentLocationBtn: UIButton!
    @IBOutlet weak var mainMapView: MKMapView!
    @IBOutlet weak var wifiView: UIView!
    @IBOutlet weak var wifiLbl: UILabel!
    
    var locationManager = CLLocationManager()
    var data = [GeofenceData]()
    var reachability = Reachability()!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.locationManager.delegate = self
        self.locationManager.requestAlwaysAuthorization()
        self.loadAllData()
        
        self.currentLocationBtn.addTarget(self, action: #selector(viewCurrentUserLocation), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkConnection(note:)), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do {
            try reachability.startNotifier()
        }
        catch {
            print("start notifier fail")
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addData" {
            let navigationController = segue.destination as! UINavigationController
            let vc = navigationController.viewControllers.first as! AddDataTableViewController
            vc.delegate = self
        }
    }
    
    @objc func viewCurrentUserLocation(){
        self.mainMapView.getUserLocation()
    }
    
    @objc func checkConnection(note: Notification) {
        let reachability = note.object as! Reachability
        
        switch reachability.connection {
        case .wifi:
            let wifiInfo = getCurrentSSIDs()
            self.changeAttributeColor(color: .green, textColor: .black)
            self.wifiLbl.text = "WiFi connected to \"\(wifiInfo[0])\""
        case .cellular:
            self.changeAttributeColor(color: .blue, textColor: .white)
            self.wifiLbl.text = "WiFi connected to \"Mobile Data\""
        case .none:
            self.changeAttributeColor(color: .red, textColor: .white)
            self.wifiLbl.text = "No internet connectivity"
        }
    }
    
    func changeAttributeColor(color: UIColor, textColor: UIColor){
        UIView.animate(withDuration: 1.0, animations: {
            self.wifiView.backgroundColor = color
            self.wifiLbl.textColor = textColor
        })
    }
    
    //load saved data
    func loadAllData(){
        self.data.removeAll()
        let allSavedData = GeofenceData.getAllData()
        allSavedData.forEach { self.add($0) }
    }
    
    //save new data
    func saveData() {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(self.data)
            UserDefaults.standard.set(data, forKey: "savedData")
        } catch {
            print("error encoding geotifications")
        }
    }
    
    //remove data
    func removeData(_ geofenceData: GeofenceData) {
        guard let index = data.firstIndex(of: geofenceData) else { return }
        self.data.remove(at: index)
        self.mainMapView.removeAnnotation(geofenceData)
        self.removeRadiusOnMap(forGeotification: geofenceData)
        self.updateGeofenceCount()
    }
    
    //add geofence to the map
    func add(_ geofenceData: GeofenceData){
        self.data.append(geofenceData)
        self.mainMapView.addAnnotation(geofenceData)
        self.drawRadiusOnMap(forGeofencing: geofenceData)
        self.startMonitor(geofenceData: geofenceData)
        self.checkDistanceUser(geofenceData: geofenceData)
        self.updateGeofenceCount()
    }
    
    //update geofence count and put to page title
    func updateGeofenceCount() {
        self.title = self.data.count > 1 ? "Map: \(self.data.count) Geofences" : "Map: \(self.data.count) Geofence"
        self.navigationItem.rightBarButtonItem?.isEnabled = (self.data.count < 10)
    }
    
    // Draw radius
    func drawRadiusOnMap(forGeofencing geofenceData: GeofenceData) {
        self.mainMapView?.addOverlay(MKCircle(center: geofenceData.coordinate, radius: geofenceData.radius))
    }
    
    //Remove radius
    func removeRadiusOnMap(forGeotification geofenceData: GeofenceData) {
        guard let overlays = self.mainMapView?.overlays else { return }
        for overlay in overlays {
            guard let circleOverlay = overlay as? MKCircle else { continue }
            let coord = circleOverlay.coordinate
            if coord.latitude == geofenceData.coordinate.latitude && coord.longitude == geofenceData.coordinate.longitude && circleOverlay.radius == geofenceData.radius {
                self.mainMapView?.removeOverlay(circleOverlay)
                break
            }
        }
    }
    
    //set region setting to notify the user when user enter / out from geofence radius
    func region(with geofenceData: GeofenceData) -> CLCircularRegion {
        let region = CLCircularRegion(center: geofenceData.coordinate, radius: geofenceData.radius, identifier: geofenceData.dataID)
        region.notifyOnEntry = true
        region.notifyOnExit = true
        
        return region
    }
    
    func startMonitor(geofenceData: GeofenceData) {
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            showAlert(withTitle:"Error", message: "This device are not supported geofencing")
            return
        }
        
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            let message = "Please change the location access for this app to Always for geofence."
            showAlert(withTitle:"Warning", message: message)
        }
        
        let fenceRegion = region(with: geofenceData)
        locationManager.startMonitoring(for: fenceRegion)
    }
    
    func checkDistanceUser(geofenceData: GeofenceData){
        guard let currentLocation: CLLocation = locationManager.location else { return }
        let geofenceLocation: CLLocation = CLLocation(latitude: geofenceData.coordinate.latitude,
                                                       longitude: geofenceData.coordinate.longitude)
        
        if currentLocation.distance(from: geofenceLocation) > geofenceData.radius {
            let connectedWifiSSID = getCurrentSSIDs()
            let connectedWifiBSSID = getCurrentBSSIDs()
            if(connectedWifiSSID[0] == geofenceData.wifiSSID.trailingSpacesTrimmed() || connectedWifiBSSID[0] == geofenceData.wifiBSSID.trailingSpacesTrimmed()){
                showAlert(withTitle:"", message: "Inside")
            }else{
                showAlert(withTitle:"", message: "Outside")
            }
        }else{
            showAlert(withTitle:"", message: "Inside")
        }
    }
    
    func stopMonitor(geofenceData: GeofenceData) {
        for region in locationManager.monitoredRegions {
            guard let circularRegion = region as? CLCircularRegion, circularRegion.identifier == geofenceData.dataID else { continue }
            locationManager.stopMonitoring(for: circularRegion)
        }
    }
    
    //AddDataTableViewControllerDelegate delegate
    func addDataTableViewController(_ controller: AddDataTableViewController, didAddCoordinate coordinate: CLLocationCoordinate2D, radius: Double, dataID: String, wifiSSID: String, wifiBSSID: String) {
        
        controller.dismiss(animated: true, completion: nil)
        let radiusTemp = min(radius, locationManager.maximumRegionMonitoringDistance)
        let geofenceData = GeofenceData(coordinate: coordinate, radius: radiusTemp, dataID: dataID, wifiSSID: wifiSSID, wifiBSSID: wifiBSSID)
        self.add(geofenceData)
        self.startMonitor(geofenceData: geofenceData)
        self.saveData()
    }
    
    //location manager delegate
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.mainMapView.showsUserLocation = status == .authorizedAlways
    }
    
    //MKMapViewDelegate delegate
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "myGeofence"
        if annotation is GeofenceData {
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
            if annotationView == nil {
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView?.canShowCallout = true
                annotationView?.pinTintColor = .purple
                let removeButton = UIButton(type: .custom)
                removeButton.frame = CGRect(x: 0, y: 0, width: 23, height: 23)
                removeButton.setImage(UIImage(named: "deleteIcon")!, for: .normal)
                annotationView?.leftCalloutAccessoryView = removeButton
            } else {
                annotationView?.annotation = annotation
            }
            return annotationView
        }
        return nil
    }
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.lineWidth = 1.0
            circleRenderer.strokeColor = .purple
            circleRenderer.fillColor = UIColor.purple.withAlphaComponent(0.4)
            return circleRenderer
        }
        return MKOverlayRenderer(overlay: overlay)
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        // Delete geotification
        let geofenceData = view.annotation as! GeofenceData
        self.removeData(geofenceData)
        self.saveData()
    }
}

extension String{
    func trailingSpacesTrimmed() -> String {
        var newString = self
        
        while newString.last?.isWhitespace == true {
            newString = String(newString.dropLast())
        }
        
        return newString
    }
}
