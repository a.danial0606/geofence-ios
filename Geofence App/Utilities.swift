//
//  Utilities.swift
//  Geofence App
//
//  Created by Ahmad Danial bin Mohd Fajariatudin on 11/09/2019.
//  Copyright © 2019 Ahmad Danial bin Mohd Fajariatudin. All rights reserved.
//

import Foundation
import MapKit
import SystemConfiguration.CaptiveNetwork

// MARK: Helper Extensions
extension UIViewController {
    func showAlert(withTitle title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}

extension MKMapView {
    func getUserLocation() {
        guard let coordinate = userLocation.location?.coordinate else { return }
        let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: 10000, longitudinalMeters: 10000)
        setRegion(region, animated: true)
        
    }
}

func getCurrentSSIDs() -> [String] {
    guard let interfaceNames = CNCopySupportedInterfaces() as? [String] else {
        return []
    }
    return interfaceNames.compactMap { name in
        guard let info = CNCopyCurrentNetworkInfo(name as CFString) as? [String:AnyObject] else {
            return nil
        }
        guard let ssid = info[kCNNetworkInfoKeySSID as String] as? String else {
            return nil
        }
        
        return ssid
    }
}


func getCurrentBSSIDs() -> [String] {
    guard let interfaceNames = CNCopySupportedInterfaces() as? [String] else {
        return []
    }
    return interfaceNames.compactMap { name in
        guard let info = CNCopyCurrentNetworkInfo(name as CFString) as? [String:AnyObject] else {
            return nil
        }
        
        guard let bssid = info[kCNNetworkInfoKeyBSSID as String] as? String else {
            return nil
        }
        
        return bssid
    }
}
