//
//  AppDelegate.swift
//  Geofence App
//
//  Created by Ahmad Danial bin Mohd Fajariatudin on 11/09/2019.
//  Copyright © 2019 Ahmad Danial bin Mohd Fajariatudin. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let locationManager = CLLocationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        let options: UNAuthorizationOptions = [.badge, .sound, .alert]
        UNUserNotificationCenter.current()
            .requestAuthorization(options: options) { success, error in
                if let error = error {
                    print("Error: \(error)")
                }
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }
    
    func handleEvent(for didEnterRegion: Bool) {
        // Show an alert if application is active
        if UIApplication.shared.applicationState == .active {
            let message = didEnterRegion == true ? "Inside" : "Outside"
            window?.rootViewController?.showAlert(withTitle: nil, message: message)
        } else {
            // Otherwise present a local notification
            let body = didEnterRegion == true ? "Inside" : "Outside"
            let notificationContent = UNMutableNotificationContent()
            notificationContent.body = body
            notificationContent.sound = UNNotificationSound.default
            notificationContent.badge = UIApplication.shared.applicationIconBadgeNumber + 1 as NSNumber
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
            let request = UNNotificationRequest(identifier: "location_change",
                                                content: notificationContent,
                                                trigger: trigger)
            UNUserNotificationCenter.current().add(request) { error in
                if let error = error {
                    print("Error: \(error)")
                }
            }
        }
    }
}

extension AppDelegate: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        //handle event for notification purposes
        handleEvent(for: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        if region is CLCircularRegion {
            guard let wifiSSID = getWifiSSID(from: region.identifier) else { return }
            guard let wifiBSSID = getWifiBSSID(from: region.identifier) else { return }
            let connectedWifiSSID = getCurrentSSIDs()
            let connectedWifiBSSID = getCurrentBSSIDs()
            if(connectedWifiSSID[0] == wifiSSID.trailingSpacesTrimmed() || connectedWifiBSSID[0] == wifiBSSID.trailingSpacesTrimmed()){
                handleEvent(for: true)
            }else{
                handleEvent(for: false)
            }
        }
    }
    
    func getWifiSSID(from identifier: String) -> String? {
        let geofenceData = GeofenceData.getAllData()
        guard let matched = geofenceData.filter({
            $0.dataID == identifier
        }).first else { return nil }
        return matched.wifiSSID
    }
    
    func getWifiBSSID(from identifier: String) -> String? {
        let geofenceData = GeofenceData.getAllData()
        guard let matched = geofenceData.filter({
            $0.dataID == identifier
        }).first else { return nil }
        return matched.wifiBSSID
    }
}
