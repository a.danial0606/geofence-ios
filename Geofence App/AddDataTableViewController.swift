//
//  AddDataTableViewController.swift
//  Geofence App
//
//  Created by Ahmad Danial bin Mohd Fajariatudin on 11/09/2019.
//  Copyright © 2019 Ahmad Danial bin Mohd Fajariatudin. All rights reserved.
//

import UIKit
import MapKit

protocol AddDataTableViewControllerDelegate {
    func addDataTableViewController(_ controller: AddDataTableViewController, didAddCoordinate coordinate: CLLocationCoordinate2D,
                                        radius: Double, dataID: String, wifiSSID: String, wifiBSSID: String)
}

class AddDataTableViewController: UITableViewController, MKMapViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var markerImgView: UIImageView!
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet var dataTableView: UITableView!
    
    @IBOutlet weak var radiusLbl: UILabel!
    @IBOutlet weak var radiusTxtField: UITextField!
    
    @IBOutlet weak var wifiSSIDLbl: UILabel!
    @IBOutlet weak var wifiSSIDTxtField: UITextField!
    
    @IBOutlet weak var wifiBSSIDLbl: UILabel!
    @IBOutlet weak var wifiBSSIDTxtField: UITextField!
    
    @IBOutlet weak var getCurrentLocationBtn: UIButton!
    @IBOutlet weak var saveBtn: UIBarButtonItem!
    
    var delegate: AddDataTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.radiusTxtField.delegate = self
        self.wifiSSIDTxtField.delegate = self
        self.wifiBSSIDTxtField.delegate = self
        
        self.dataTableView.tableFooterView = UIView()
        self.mapView.delegate = self
        
        self.title = "Add Data"
        
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style:.plain, target: self, action:nil)
        
        self.getCurrentLocationBtn.addTarget(self, action: #selector(getCurrentLocation), for: .touchUpInside)
        self.saveBtn.isEnabled = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    @IBAction func cancelBtnAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveBtnAction(_ sender: UIBarButtonItem) {
        let coordinate = mapView.centerCoordinate
        let radius = Double(radiusTxtField.text!) ?? 0
        let dataID = NSUUID().uuidString
        let wifiSSID = self.wifiSSIDTxtField.text!
        let wifiBSSID = self.wifiBSSIDTxtField.text!
        delegate?.addDataTableViewController(self, didAddCoordinate: coordinate, radius: radius, dataID: dataID, wifiSSID: wifiSSID, wifiBSSID: wifiBSSID)
    }
    
    @objc func getCurrentLocation(){
        mapView.getUserLocation()
    }
    
    @objc func keyboardWillAppear() {
        //Do something here
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillDisappear() {
        //Do something here
        view.gestureRecognizers?.removeAll()
    }

    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.saveBtn.isEnabled = !radiusTxtField.text!.isEmpty && (!wifiSSIDTxtField.text!.isEmpty || !wifiBSSIDTxtField.text!.isEmpty)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        if textField == radiusTxtField {
            textField.resignFirstResponder()
            wifiSSIDTxtField.becomeFirstResponder()
        } else if textField == wifiSSIDTxtField {
            textField.resignFirstResponder()
            wifiBSSIDTxtField.becomeFirstResponder()
        } else if textField == wifiBSSIDTxtField {
            textField.resignFirstResponder()
        }
        return true
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }
    

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
