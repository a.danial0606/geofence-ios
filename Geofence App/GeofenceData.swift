//
//  GeofenceData.swift
//  Geofence App
//
//  Created by Ahmad Danial bin Mohd Fajariatudin on 11/09/2019.
//  Copyright © 2019 Ahmad Danial bin Mohd Fajariatudin. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

class GeofenceData: NSObject, Codable, MKAnnotation {
    
    enum CodingKeys: String, CodingKey {
        case latitude, longitude, radius, dataID, wifiSSID, wifiBSSID
    }
    
    var coordinate: CLLocationCoordinate2D
    var radius: CLLocationDistance
    var dataID: String
    var wifiSSID: String
    var wifiBSSID: String
    
    var title: String? {
        return "WiFi Name: \(wifiSSID)"
    }
    
    var subtitle: String? {
        return "Radius: \(radius)m"
    }
    
    init(coordinate: CLLocationCoordinate2D, radius: CLLocationDistance, dataID: String, wifiSSID: String, wifiBSSID: String) {
        self.coordinate = coordinate
        self.radius = radius
        self.dataID = dataID
        self.wifiSSID = wifiSSID
        self.wifiBSSID = wifiBSSID
    }
    
    // MARK: Codable
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let latitude = try values.decode(Double.self, forKey: .latitude)
        let longitude = try values.decode(Double.self, forKey: .longitude)
        coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        radius = try values.decode(Double.self, forKey: .radius)
        dataID = try values.decode(String.self, forKey: .dataID)
        wifiSSID = try values.decode(String.self, forKey: .wifiSSID)
        wifiBSSID = try values.decode(String.self, forKey: .wifiBSSID)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(coordinate.latitude, forKey: .latitude)
        try container.encode(coordinate.longitude, forKey: .longitude)
        try container.encode(radius, forKey: .radius)
        try container.encode(dataID, forKey: .dataID)
        try container.encode(wifiSSID, forKey: .wifiSSID)
        try container.encode(wifiBSSID, forKey: .wifiBSSID)
    }
    
}

extension GeofenceData {
    public class func getAllData() -> [GeofenceData] {
        guard let savedData = UserDefaults.standard.data(forKey: "savedData") else { return [] }
        let decoder = JSONDecoder()
        if let savedGeotifications = try? decoder.decode(Array.self, from: savedData) as [GeofenceData] {
            return savedGeotifications
        }
        return []
    }
}
