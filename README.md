1. Clone this repo
2. Open terminal and cd to project directory. Run command "pod install"
2. Run using xcode (open .xcworkspace instead of .xcodeproj since I use podfile)
3. Build it on the device for WiFi connection testing
4. Enter the radius, WiFi SSID/Name or WiFi BSSID and select the location coordinate by dragging the marker on the map
5. Save the geofence
4. Use xcode location button (Idk what they called it) to mock your location for testing purpose as shown in screenshot 714 (in the circle) below
5. Happy Testing Peeps !


Screenshot 714:

![Screenshot 714](Screenshot/Screenshot_2019-09-13_at_5.06.11_PM.png)

I use ReachabilitySwift framework to detect the network connection as shown in gif below

![](gif/video1.gif)


And here the the gif of the app


Same as wifi that device connected

[Sample gif 1](https://d-bittech.com/video2.gif)


Different wifi that device connected

[Sample gif 2](https://d-bittech.com/video3.gif)

Screenshot Sample

![Screenshot 1](Screenshot/IMG_0729.PNG)

![Screenshot 2](Screenshot/IMG_0730.PNG)

![Screenshot 3](Screenshot/IMG_0731.PNG)

![Screenshot 4](Screenshot/IMG_0732.PNG)

![Screenshot 5](Screenshot/2C19124A-BD6D-4CBE-9CCB-002E683AE93E.png)
